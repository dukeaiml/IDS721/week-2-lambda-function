use tracing_subscriber::filter::{EnvFilter, LevelFilter};use lambda_http::{run, service_fn, Body, Error, Request, RequestExt, Response};
use serde_json;

#[derive(Debug, serde::Serialize)]
struct BMIResponse {
    bmi: f64,
    category: &'static str,
}

async fn bmi_handler(event: Request) -> Result<Response<Body>, Error> {
    // Extract weight and height from query parameters
    let weight: f64 = event
        .query_string_parameters_ref()
        .and_then(|params| params.first("weight"))
        .and_then(|weight| weight.parse().ok())
        .unwrap_or(0.0);

    let height: f64 = event
        .query_string_parameters_ref()
        .and_then(|params| params.first("height"))
        .and_then(|height| height.parse().ok())
        .unwrap_or(0.0);

    // Calculate BMI height in meters and weight in kg 
    let bmi: f64 = if height != 0.0 {
        weight / (height * height)
    } else {
        0.0
    };

    // Determine BMI category
    let category = if bmi < 18.5 {
        "Underweight"
    } else if bmi < 24.9 {
        "Normal weight"
    } else if bmi < 29.9 {
        "Overweight"
    } else {
        "Obese"
    };

    // Prepare the response with BMI and category
    let response = BMIResponse { bmi, category };
    
    // Serialize the response into JSON
    let response_body = serde_json::to_string(&response)?;


    // Return the response
    Ok(Response::builder()
        .status(200)
        .header("content-type", "text/html")
        //.body(Body::from(format!("BMI: {:.2}\nCategory: {}", response.bmi, response.category)))
        .body(Body::from(response_body))
        .expect("Failed to build response"))
}



#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(bmi_handler)).await
}

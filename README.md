# Rust AWS Lambda Function

## Overview
This is a simple AWS Lambda function written in Rust. It calculates Body Mass Index (BMI) based on input weight and height parameters and returns the BMI value along with the corresponding category.

## Requirements
- Rust programming language
- Cargo Lambda for AWS Lambda deployment
- AWS account with appropriate permissions to create Lambda functions and API Gateway resources

## Functionality
The Lambda function receives weight(kg) and height(m) as query parameters, calculates the BMI, determines the BMI category, and returns the result in JSON format.

## Installation and Deployment

1. Clone this repository to your local machine.
2. Ensure you have Rust and Cargo installed on your system.
3. Set up your AWS credentials and configure the AWS CLI.
4. Deploy the Lambda function using Cargo Lambda.
5. Set up API Gateway to trigger the Lambda function.

## AWS API Details

- **API Gateway Name**: testAPI
- **API Endpoint**: [https://7aiuz6gmsi.execute-api.us-east-1.amazonaws.com/prod/test-res](https://7aiuz6gmsi.execute-api.us-east-1.amazonaws.com/prod/test-res)
- **Authorization**: NONE
- **Method**: GET
- **Resource Path**: /test-res

## Usage

- Invoke the Lambda function by sending HTTP GET requests to the API Gateway endpoint with `weight` and `height` query parameters:
https://7aiuz6gmsi.execute-api.us-east-1.amazonaws.com/prod/test-res?weight=60&height=1.77
- Example response:
```json
{
  "bmi": 22.86,
  "category": "Normal weight"
}
```

## License

This project is licensed under the [MIT License](LICENSE).